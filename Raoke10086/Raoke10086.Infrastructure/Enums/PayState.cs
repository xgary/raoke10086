﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 支付状态
    /// </summary>
    public enum PayState
    {
        /// <summary>
        /// 未付款
        /// </summary>
        Unpaid = 0,

        /// <summary>
        /// 付款中
        /// </summary>
        Paying = 1,

        /// <summary>
        /// 已付款
        /// </summary>
        Paid = 2,

        /// <summary>
        /// 已退款
        /// </summary>
        Returned = 6,

        /// <summary>
        /// 部分退款
        /// </summary>
        PartialReturned = 7,
    }
}
