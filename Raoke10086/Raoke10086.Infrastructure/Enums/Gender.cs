﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    public enum Gender
    {
        /// <summary>
        /// 未知
        /// </summary>
        Unknow = 0,

        /// <summary>
        /// 男性
        /// </summary>
        Male = 1,

        /// <summary>
        /// 女性
        /// </summary>
        Female = 2,
    }
}
