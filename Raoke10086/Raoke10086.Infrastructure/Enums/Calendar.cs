﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 历法
    /// </summary>
    public enum Calendar
    {
        /// <summary>
        /// 公历
        /// </summary>
        Solar = 0,
        /// <summary>
        /// 农历
        /// </summary>
        Lunar = 1,
    }
}
