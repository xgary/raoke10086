﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 消息状态
    /// </summary>
    public enum MessageState
    {
        /// <summary>
        /// 待处理
        /// </summary>
        Pending,

        /// <summary>
        /// 处理中
        /// </summary>
        Processing,

        /// <summary>
        /// 完成
        /// </summary>
        Completed = 2,

        /// <summary>
        /// 异常
        /// </summary>
        Exception = 3,
    }
}
