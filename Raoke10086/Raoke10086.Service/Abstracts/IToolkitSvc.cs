﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Abstracts
{
    public interface IToolkitSvc:IDependency
    {
        string GetQiniuToken(string bucket);
    }
}
