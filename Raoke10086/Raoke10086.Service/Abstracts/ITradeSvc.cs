﻿using Raoke10086.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Abstracts
{
    /// <summary>
    /// 订单操作服务
    /// </summary>
    public interface ITradeSvc: IDependency
    {
        /// <summary>
        /// 提交订单
        /// </summary>
        TradeDto Create(Guid userID, PostTradeDto model);

        /// <summary>
        /// 关闭订单
        /// </summary>
        bool Close(Guid userID, Guid id);

        /// <summary>
        /// 收货
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Receipt(Guid userID, ReceiptDto model);

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="id"></param>
        TradeDto Get(Guid userID, Guid id);

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        List<TradeDto> Get(Guid userID, TradeFilter filter);
    }
}
