﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 用户消息
    /// </summary>
    [Table("Messages", Schema = "dbo")]
    public class Message
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 模板ID
        /// </summary>
        public string TemplateID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 详情页面
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 生成时间
        /// </summary>
        public DateTime GenerationTime { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime? SendTime { get; set; }

        /// <summary>
        /// 阅读时间
        /// </summary>
        public DateTime? ReadTime { get; set; }
        
        /// <summary>
        /// 状态
        /// </summary>
        public MessageState State { get; set; }

        /// <summary>
        /// 错误代码
        /// </summary>
        public string ErrCode { get; set; }

        /// <summary>
        /// 错误代码描述
        /// </summary>
        public string ErrMsg { get; set; }

        /// <summary>
        /// 消息ID
        /// </summary>
        public string MsgID { get; set; }

        /// <summary>
        /// 变量数据[JSON]
        /// {
        ///   {
        ///     "first":{
        ///       "value":"巧克力",
        ///       "color":"#173177"
        ///     }
        ///   }
        /// }
        /// </summary>
        public string Data { get; set; }


    }
}
