﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    [Table("Users", Schema = "dbo")]
    public class User
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [StringLength(32)]
        public string UserName { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [StringLength(256)]
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(16)]
        public string Phone { get; set; }

        /// <summary>
        /// 密码(bcrypt)
        /// </summary>
        [StringLength(256)]
        public string Password { get; set; }

        /// <summary>
        /// 支付密码（bcrypt）
        /// </summary>
        [StringLength(256)]
        public string PayPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SecurityStamp { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public UserState State { get; set; }

        /// <summary>
        /// 手机是否验证
        /// </summary>
        public bool PhoneConfirmed { get; set; }

        /// <summary>
        /// 邮箱是否验证
        /// </summary>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        /// 是否开启两步验证
        /// </summary>
        public bool TwoFactorEnabled { get; set; }

        /// <summary>
        /// 锁定至日期
        /// </summary>
        public DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        /// 是否启用锁定
        /// </summary>
        public bool LockoutEnabled { get; set; }

        /// <summary>
        /// 访问错误次数
        /// </summary>
        public int AccessFailedCount { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime JoinDate { get; set; }

        /// <summary>
        /// 最后登录日期
        /// </summary>
        public DateTime? Logined { get; set; }
    }
}
