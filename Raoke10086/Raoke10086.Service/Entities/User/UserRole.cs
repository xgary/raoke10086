﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 用户角色
    /// </summary>
    [Table("UserRoles", Schema = "dbo")]
    public class UserRole
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UserID { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        [Key, Column(Order = 1)]
        public string RoleKey { get; set; }
    }
}
