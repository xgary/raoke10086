﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    [Table("UserProfiles", Schema = "dbo")]
    public class UserProfile
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UserID { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [StringLength(20)]
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(30)]
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [StringLength(200)]
        public string Avatar { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 历法
        /// </summary>
        public Calendar Calendar { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        [StringLength(100)]
        public string Career { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        [StringLength(100)]
        public string Company { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>
        [StringLength(200)]
        public string Intro { get; set; }
    }
}
