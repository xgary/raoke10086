﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 供应商
    /// </summary>
    [Table("Suppliers", Schema = "dbo")]
    public class Supplier
    {
        public Guid ID { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public UserState State { get; set; }
    }
}
