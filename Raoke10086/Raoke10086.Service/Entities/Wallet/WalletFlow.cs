﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 资金流动表
    /// </summary>
    [Table("WalletFlows", Schema = "dbo")]
    public class WalletFlow
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// 交易对象
        /// </summary>
        public Guid ToUserID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// 商品ID
        /// </summary>
        public Guid ProductID { get; set; }

        /// <summary>
        /// 交易ID
        /// </summary>
        public Guid TradeID{get;set;}

        /// <summary>
        /// 交易订单号[微信]
        /// </summary>
        public string TransferPaymentNo { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
