﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 钱包
    /// </summary>
    [Table("Wallets", Schema = "dbo")]
    public class Wallet
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 现金余额
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// 冻结余额
        /// </summary>
        public decimal FrozenValue { get; set; }

        /// <summary>
        /// 已提现金额
        /// </summary>
        public decimal WithdrewValue { get; set; }

        /// <summary>
        /// 可提现金额
        /// </summary>
        public decimal WithdrawBalance { get; set; }

        /// <summary>
        /// 总收入
        /// </summary>
        public decimal TotalIncome { get; set; }

        /// <summary>
        /// 总支出
        /// </summary>
        public decimal TotalExpense { get; set; }
    }
}
