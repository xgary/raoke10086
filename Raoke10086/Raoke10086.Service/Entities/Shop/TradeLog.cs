﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 订单记录
    /// </summary>
    [Table("TradeLogs", Schema = "dbo")]
    public class TradeLog
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// 交易ID
        /// </summary>
        public Guid TradeID { get; set; }

        /// <summary>
        /// 记录描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
