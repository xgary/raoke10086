﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    [Table("Products", Schema = "dbo")]
    public class Product
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 分类ID
        /// </summary>
        public Guid CategaryID { get; set; }

        /// <summary>
        /// 标签[JSON]
        /// [{"id":1,"name":"电信","style":"color:#428bca"},{"id":38,"name":"成都","style":"color:#428bca"}]
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        [StringLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        [StringLength(200)]
        public string Cover { get; set; }

        /// <summary>
        /// 商品封面图片集
        /// </summary>
        [StringLength(3000)]
        public string Galleries { get; set; }

        /// <summary>
        /// 商家提示
        /// </summary>
        [StringLength(1000)]
        public string NoteMessage { get; set; }

        /// <summary>
        /// 商品留言
        /// type[text|tel|email|date|time|id_no|image]
        /// [{"name":"手机号","type":"tel","multiple":"0","required":"1"}]
        /// </summary>
        [StringLength(300)]
        public string Messages { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal OriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal SalePrice { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal PostPrice { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// 已售数量
        /// </summary>
        public int Sold { get; set; }

        /// <summary>
        /// 是否是虚拟商品
        /// </summary>
        public bool IsVirtual { get; set; }

        /// <summary>
        /// 服务提供商
        /// </summary>
        public ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 城市Code
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 商品状态
        /// </summary>
        public ProductState State { get; set; }

        /// <summary>
        /// 商品简介
        /// </summary>
        [StringLength(500)]
        public string Introduction { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [MaxLength]
        public string Details { get; set; }
    }
}
