﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 产品分类
    /// </summary>
    [Table("Categories", Schema = "dbo")]
    public class Category
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        /// <summary>
        /// 父分类
        /// </summary>
        public Guid PID { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Style
        /// </summary>
        public string Style { get; set; }
    }
}
