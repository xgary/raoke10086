﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 订单
    /// </summary>
    [Table("Trades", Schema = "dbo")]
    public class Trade
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }

        [StringLength(32)]
        public string No { get; set; }

        /// <summary>
        /// 外部交易订单
        /// </summary>
        [StringLength(128)]
        public string ExternalID { get; set; }

        #region 商品冗余信息
        /// <summary>
        /// 商品ID
        /// </summary>
        public Guid ProductID { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal ProductOriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal ProductSalePrice { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        [StringLength(100)]
        public string ProductTitle { get; set; }

        /// <summary>
        /// 商品封面
        /// </summary>
        [StringLength(100)]
        public string ProductCover { get; set; }

        #endregion
        
        /// <summary>
        /// 商品数量
        /// </summary>
        public double Number { get; set; }

        /// <summary>
        /// 交易价格
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal Postage { get; set; }

        /// <summary>
        /// 买家留言
        ///  [{"name":"手机号","value":"18682759981"}]
        /// </summary>
        [StringLength(300)]
        public string Message { get; set; }

        /// <summary>
        /// 买家备注
        /// </summary>
        public string Memo { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }

        /// <summary>
        /// 收件人电话
        /// </summary>
        public string ReceiverPhone { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceiverAddress { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        public string AirWayBillNo{get;set;}

        /// <summary>
        /// 购买者ID
        /// </summary>
        public Guid BuyerID { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public TradeState State { get; set; }

        /// <summary>
        /// 付款状态
        /// </summary>
        public PayState PayState { get; set; }

        /// <summary>
        /// 配送状态
        /// </summary>
        public ShipmentState ShipmentState { get; set; }

        /// <summary>
        /// 订单生成时间
        /// </summary>
        public DateTime GenerationTime { get; set; }

        /// <summary>
        /// 订单确认时间
        /// </summary>
        public DateTime? ConfirmTime { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PayTime { get; set; }

        /// <summary>
        /// 订单发货时间
        /// </summary>
        public DateTime? DeliveryTime { get; set; }

        /// <summary>
        /// 订单处理完成时间
        /// </summary>
        public DateTime? FinishTime { get; set; }
    }
}
