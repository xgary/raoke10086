﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace Raoke10086.Service.Entities
{
    public class Raoke10086Context:DbContext
    {

        static Raoke10086Context()
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Raoke10086Context, Migrations.Configuration>());
        }

        /// <summary>
        /// 构造器
        /// </summary>
        public Raoke10086Context() : base("Raoke10086DB")
        {

        }

        #region User
        public IDbSet<User> Users { get; set; }

        public IDbSet<UserProfile> UserProfiles { get; set; }

        public IDbSet<UserLogin> UserLogins { get; set; }

        public IDbSet<Role> Roles { get; set; }

        public IDbSet<UserRole> UserRoles { get; set; }


        #endregion

        #region Shop
        public IDbSet<Category> Categories { get; set; }

        public IDbSet<Product> Products { get; set; }

        public IDbSet<Trade> Trades { get; set; }

        public IDbSet<TradeLog> TradeLogs { get; set; }

        #endregion

        #region Wallet
        public IDbSet<Wallet> Wallets { get; set; }

        public IDbSet<WalletFlow> WalletFlows { get; set; }

        public IDbSet<Withdrawal> Withdrawals { get; set; }
        #endregion

        #region Parther
        public IDbSet<App> Apps { get; set; }

        public IDbSet<Agent> Agents { get; set; }

        public IDbSet<Supplier> Suppliers { get; set; }
        #endregion

        #region System
        public IDbSet<Tag> Tags { get; set; }

        public IDbSet<City> Cities { get; set; }

        public IDbSet<Message> Messages { get; set; }

        public IDbSet<MessageTemplate> MessageTemplates { get; set; }
        #endregion

    }
}
