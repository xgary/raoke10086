namespace Raoke10086.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Apps",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Secret = c.String(),
                        CallbackURL = c.String(),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PID = c.Guid(nullable: false),
                        Name = c.String(),
                        Style = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        LCID = c.String(nullable: false, maxLength: 128),
                        Code = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        FullName = c.String(),
                        ParentCode = c.String(),
                    })
                .PrimaryKey(t => new { t.LCID, t.Code });
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TemplateID = c.String(),
                        Title = c.String(),
                        Content = c.String(),
                        Url = c.String(),
                        GenerationTime = c.DateTime(nullable: false),
                        SendTime = c.DateTime(),
                        ReadTime = c.DateTime(),
                        State = c.Int(nullable: false),
                        ErrCode = c.String(),
                        ErrMsg = c.String(),
                        MsgID = c.String(),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MessageTemplates",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 32),
                        WechatTemplateID = c.String(maxLength: 100),
                        Title = c.String(maxLength: 50),
                        Content = c.String(maxLength: 500),
                        Url = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategaryID = c.Guid(nullable: false),
                        Tags = c.String(),
                        Title = c.String(maxLength: 100),
                        Cover = c.String(maxLength: 200),
                        Galleries = c.String(maxLength: 3000),
                        NoteMessage = c.String(maxLength: 1000),
                        Messages = c.String(maxLength: 300),
                        OriginPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Stock = c.Int(nullable: false),
                        Sold = c.Int(nullable: false),
                        IsVirtual = c.Boolean(nullable: false),
                        ServiceProvider = c.Int(nullable: false),
                        CityCode = c.String(),
                        State = c.Int(nullable: false),
                        Introduction = c.String(maxLength: 500),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128),
                        Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Key);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Style = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TradeLogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TradeID = c.Guid(nullable: false),
                        Description = c.String(),
                        UserID = c.Guid(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Trades",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        No = c.String(maxLength: 32),
                        ExternalID = c.String(maxLength: 128),
                        ProductID = c.Guid(nullable: false),
                        ProductOriginPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductSalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductTitle = c.String(maxLength: 100),
                        ProductCover = c.String(maxLength: 100),
                        Number = c.Double(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Postage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Message = c.String(maxLength: 300),
                        Memo = c.String(),
                        ReceiverName = c.String(),
                        ReceiverPhone = c.String(),
                        ReceiverAddress = c.String(),
                        AirWayBillNo = c.String(),
                        BuyerID = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        PayState = c.Int(nullable: false),
                        ShipmentState = c.Int(nullable: false),
                        GenerationTime = c.DateTime(nullable: false),
                        ConfirmTime = c.DateTime(),
                        PayTime = c.DateTime(),
                        DeliveryTime = c.DateTime(),
                        FinishTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        UserID = c.Guid(nullable: false),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        UserID = c.Guid(nullable: false),
                        RealName = c.String(maxLength: 20),
                        Nickname = c.String(maxLength: 30),
                        Avatar = c.String(maxLength: 200),
                        Gender = c.Int(nullable: false),
                        Calendar = c.Int(nullable: false),
                        Birthday = c.DateTime(),
                        Career = c.String(maxLength: 100),
                        Company = c.String(maxLength: 100),
                        Intro = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserID = c.Guid(nullable: false),
                        RoleKey = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserID, t.RoleKey });
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserName = c.String(maxLength: 32),
                        Email = c.String(maxLength: 256),
                        Phone = c.String(maxLength: 16),
                        Password = c.String(maxLength: 256),
                        PayPassword = c.String(maxLength: 256),
                        SecurityStamp = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        PhoneConfirmed = c.Boolean(nullable: false),
                        EmailConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        JoinDate = c.DateTime(nullable: false),
                        Logined = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WalletFlows",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ToUserID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        TradeID = c.Guid(nullable: false),
                        TransferPaymentNo = c.String(),
                        Description = c.String(),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Wallets",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FrozenValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WithdrewValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WithdrawBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalIncome = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalExpense = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Withdrawals",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 32),
                        UserID = c.Guid(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        GenerationTime = c.DateTime(nullable: false),
                        TransferTime = c.DateTime(),
                        TransferPaymentNo = c.String(),
                        TransferPaymentTime = c.DateTime(),
                        TransferResultCode = c.String(),
                        TransferErrCode = c.String(),
                        TransferErrCodeDes = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Withdrawals");
            DropTable("dbo.Wallets");
            DropTable("dbo.WalletFlows");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.Trades");
            DropTable("dbo.TradeLogs");
            DropTable("dbo.Tags");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Roles");
            DropTable("dbo.Products");
            DropTable("dbo.MessageTemplates");
            DropTable("dbo.Messages");
            DropTable("dbo.Cities");
            DropTable("dbo.Categories");
            DropTable("dbo.Apps");
            DropTable("dbo.Agents");
        }
    }
}
