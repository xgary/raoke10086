﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Models
{
    /// <summary>
    /// 用户资料
    /// </summary>
    public class UserProfileDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public UserState State { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 历法
        /// </summary>
        public Calendar Calendar { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public string Career { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>
        public string Intro { get; set; }
    }

    public class CreateUserDto
    {
        #region 用户账户
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 密码(bcrypt)
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 支付密码(bcrypt)
        /// </summary>
        public string PayPassword { get; set; }

        /// <summary>
        /// 手机是否验证
        /// </summary>
        public bool PhoneConfirmed { get; set; }

        /// <summary>
        /// 邮箱是否验证
        /// </summary>
        public bool EmailConfirmed { get; set; }
        #endregion

        #region 用户基本资料
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 历法
        /// </summary>
        public Calendar Calendar { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public string Career { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>
        public string Intro { get; set; }
        #endregion

        #region 第三方登录
        /// <summary>
        /// 登录方
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        /// 用户OpenID
        /// </summary>
        public string ProviderKey { get; set; }
        #endregion
    }

    /// <summary>
    /// 用户更新资料
    /// </summary>
    public class UpdateUserProfileDto
    {
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 历法
        /// </summary>
        public Calendar Calendar { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public string Career { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>
        public string Intro { get; set; }
    }

    /// <summary>
    /// 用户第三方登录
    /// </summary>
    public class ExternalLoginDto
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    /// <summary>
    /// 用户登录
    /// </summary>
    public class LoginDto
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }

}
