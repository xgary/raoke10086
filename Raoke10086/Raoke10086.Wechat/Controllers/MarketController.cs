﻿using Raoke10086.Service.Abstracts;
using Raoke10086.Wechat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Raoke10086.Wechat.Controllers
{
    /// <summary>
    /// 分润平台
    /// </summary>
    [RoutePrefix("market")]
    public class MarketController : Controller
    {
        private readonly IUserSvc _userSvc;

        public MarketController(IUserSvc userSvc)
        {
            _userSvc = userSvc;
        }


        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("")]
        public ActionResult Index()
        {
            var model = new MarketIndexViewModel();
            var profile = _userSvc.Get(this.User.Identity.GetUserID());
            model.UserID = profile.ID;
            model.Nickname = profile.Nickname;
            model.Avatar = profile.Avatar;
            return View(model);
        }

        /// <summary>
        /// 下级代理
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("agent")]
        public ActionResult Agent()
        {
            return View();
        }

        /// <summary>
        /// 返利记录
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("rebate")]
        public ActionResult Rebate()
        {
            return View();
        }

        /// <summary>
        /// 生成推广海报
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("poster")]
        public ActionResult Poster()
        {
            return View();
        }

        /// <summary>
        /// 生成推广海报-选择页
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("poster-choose")]
        public ActionResult PosterChoose()
        {
            return View();
        }

        /// <summary>
        /// 生成推广海报-二维码
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("qr-poster")]
        public ActionResult QrPoster()
        {
            return View();
        }

        /// <summary>
        /// 转出奖励
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("roll")]
        public ActionResult Roll()
        {
            return View();
        }
    }
}