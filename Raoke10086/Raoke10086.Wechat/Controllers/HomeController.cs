﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Raoke10086.Wechat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //待收货
        public ActionResult Delivery()
        {
            return View();
        }

        //信息中心
        public ActionResult Message()
        {
            return View();
        }
        //个人中心
        public ActionResult MyCenter()
        {
            return View();
        }
        //待发货
        public ActionResult Receive()
        {
            return View();
        }
        //充值记录
        public ActionResult Record()
        {
            return View();
        }

    }
}