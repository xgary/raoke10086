﻿using Raoke10086.Infrastructure.Enums;
using Raoke10086.Service.Abstracts;
using Raoke10086.Wechat.ViewModels;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Raoke10086.Wechat.Controllers
{
    /// <summary>
    /// 账户页
    /// </summary>
    [RoutePrefix("account")]
    public class AccountController : Controller
    {
        private readonly IUserSvc _userSvc;

        public AccountController(IUserSvc userSvc)
        {
            _userSvc = userSvc;
        }

        /// <summary>
        /// 个人首页
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("")]
        public ActionResult Index()
        {
            var profile = _userSvc.Get(this.User.Identity.GetUserID());
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet,Route("login")]
        public ActionResult LogIn(string code,string state,string returnUrl)
        {
            if (string.IsNullOrEmpty(code))
            {
                var redirectUrl = new UrlHelper(Request.RequestContext)
                .Action("login",
                        "account",
                        new RouteValueDictionary()
                        {
                                    {"returnUrl", returnUrl}
                        },
                        HttpContext.Request.Url.Scheme);

                var loginUrl = OAuthApi.GetAuthorizeUrl(Current.WechatConfig.AppID,
                                                        redirectUrl,
                                                        Current.WechatConfig.State,
                                                        OAuthScope.snsapi_userinfo);
                return Redirect(loginUrl);
            }

            if (state != Current.WechatConfig.State)
            {
                return Content("验证失败,请从正规途径进入。");
            }

            var result = OAuthApi.GetAccessToken(Current.WechatConfig.AppID, Current.WechatConfig.AppSecret, code);
            if (result.errcode != ReturnCode.请求成功)
            {
                return Content("错误：" + result.errmsg);
            }
            OAuthUserInfo userInfo = null;
            try
            {
                userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                var userProfile = _userSvc.ExternalLogin(new Service.Models.ExternalLoginDto
                {
                    LoginProvider = "wechat",
                    ProviderKey = userInfo.openid,
                });
                if (userProfile != null)
                {
                    var identity = new ClaimsIdentity(new[] 
                    {
                        new Claim("id", userProfile.ID.ToString()),
                        new Claim("nickname", userProfile.Nickname),
                        new Claim("avatar", userProfile.Avatar),
                    }, "ApplicationCookie");

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;

                    authManager.SignIn(identity);
                    return Redirect(returnUrl);
                }
                return View("Create", new CreateAccountViewModel
                {
                    Avatar = userInfo.headimgurl,
                    Gender = (Gender)userInfo.sex,
                    LoginProvider = "wechat",
                    ProviderKey = userInfo.openid,
                    Nickname = userInfo.nickname,
                });
            }
            catch (ErrorJsonResultException ex)
            {
                return Content(ex.Message);
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet,Route("create")]
        public ActionResult Create(string code, string state)
        {
            if(string.IsNullOrEmpty(code))
            {
                return Content("授权被拒绝");
            }

            if (state != Current.WechatConfig.State)
            {
                return Content("验证失败,请从正规途径进入。");
            }
            
            var result = OAuthApi.GetAccessToken(Current.WechatConfig.AppID, Current.WechatConfig.AppSecret, code);
            if (result.errcode != ReturnCode.请求成功)
            {
                return Content("错误：" + result.errmsg);
            }
            OAuthUserInfo userInfo = null;
            try
            {
                userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                return View(new CreateAccountViewModel
                {
                    Avatar=userInfo.headimgurl,
                    Gender=(Gender)userInfo.sex,
                    LoginProvider="wechat",
                    ProviderKey=userInfo.openid,
                    Nickname=userInfo.nickname,
                });
            }
            catch (ErrorJsonResultException ex)
            {
                return Content(ex.Message);
            }
           
        }

        /// <summary>
        /// 创建账户
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost,Route("create")]
        public ActionResult Create(CreateAccountViewModel model)
        {
            _userSvc.Create(new Service.Models.CreateUserDto
            {
                RealName=model.RealName,
                Phone=model.Phone,
                Password = model.Password,

                LoginProvider =model.LoginProvider,
                ProviderKey=model.ProviderKey,
                Nickname=model.Nickname,
                Avatar=model.Avatar,
                Gender=model.Gender,
                
            });

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 个人资料设置
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("setting")]
        public ActionResult Setting()
        {
            return View();
        }

        /// <summary>
        /// 钱包
        /// 钱包余额+钱包交易记录
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("wallet")]
        public ActionResult Wallet()
        {
            return View();
        }
    }
}