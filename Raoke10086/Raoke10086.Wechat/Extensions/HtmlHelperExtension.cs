﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
namespace Raoke10086.Wechat.Extensions
{
    public static class HtmlHelperExtension
    {
        public static MvcHtmlString Tags(this HtmlHelper html, string stringTags)
        {
            if (string.IsNullOrWhiteSpace(stringTags))
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return MvcHtmlString.Create(string.Join(" ", JArray.Parse(stringTags).Select(tag =>
                {
                    var em = new TagBuilder("em");
                    em.IdAttributeDotReplacement = "tag" + tag.Value<int>("id");
                    em.SetInnerText(tag.Value<string>("name"));
                    em.Attributes["style"] = tag.Value<string>("style");
                    return em;
                })));
            }
        }
    }
}