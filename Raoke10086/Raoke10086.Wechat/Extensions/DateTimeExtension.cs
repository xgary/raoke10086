﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.Extensions
{
    public static class DateTimeExtension
    {
        private readonly static string[] dayNames = new[] { "今天", "明天", "后天","三天后","四天后","五天后","一周后" };

        public static string Delivery(this DateTime dt,int day)
        {
            return string.Format("预计{0}({1})送达", dayNames[Math.Min(6, day)], dt.AddDays(day).ToString("MM月dd日"));
        }
    }
}