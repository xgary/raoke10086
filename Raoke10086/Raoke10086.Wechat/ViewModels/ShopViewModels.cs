﻿using Raoke10086.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.ViewModels
{
    /// <summary>
    /// 商品详情页
    /// </summary>
    public class ShopDetailViewModel
    {
        /// <summary>
        /// 商品信息
        /// </summary>
        public ProductDto Product { get; set; }


    }

    /// <summary>
    /// 商品下单页面
    /// </summary>
    public class ShopTradeViewModel
    {
        /// <summary>
        /// 商品信息
        /// </summary>
        public ProductDto Product { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; }
    }
}