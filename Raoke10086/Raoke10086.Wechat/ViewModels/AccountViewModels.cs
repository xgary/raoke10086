﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.ViewModels
{
    public class CreateAccountViewModel
    {
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 短信验证码
        /// </summary>
        public string VerifyCode { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 登录方
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        /// 用户OpenID
        /// </summary>
        public string ProviderKey { get; set; }
    }
}