﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.Models
{
    public class ShopHaomaFilter
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public ServiceProvider Provider { get; set; }
    }
}