﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.Models
{
    public class WechatConfig
    {
        public string AppID { get; set; }

        public string AppSecret { get; set; }

        public string State { get; set; }
    }
}