﻿using Raoke10086.Wechat.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat
{
    public static class Current
    {
        static Current()
        {
            //默认地区配置
            CityCode = "cd";

            //初始化微信配置
            WechatConfig = new WechatConfig
            {
                AppID = ConfigurationManager.AppSettings["Wechat.AppID"],
                AppSecret = ConfigurationManager.AppSettings["Wechat.AppSecret"],
                State = ConfigurationManager.AppSettings["Wechat.State"],
            };

            //初始化商品分类配置
            CategoryConfig = new CategoryConfig
            {
                Haoma = Guid.Empty
            };
        }
        /// <summary>
        /// 当地语言编码
        /// </summary>
        public static string LCID { get; set; }

        /// <summary>
        /// 默认城市CODE
        /// </summary>
        public static string CityCode { get; set; }

        /// <summary>
        /// 微信配置
        /// </summary>
        public static WechatConfig WechatConfig { get; set; }

        /// <summary>
        /// 分类配置
        /// </summary>
        public static CategoryConfig CategoryConfig { get; set; }
    }
}