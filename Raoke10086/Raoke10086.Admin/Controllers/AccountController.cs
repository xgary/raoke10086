﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Raoke10086.Admin.Models;
using Raoke10086.Service.Abstracts;

namespace Raoke10086.Admin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserSvc _userSvc;

        public AccountController(IUserSvc userSvc)
        {
            _userSvc = userSvc;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userProfile = _userSvc.Login(new Service.Models.LoginDto
            {
                Email = model.Email,
                Password = model.Password
            });

            if (userProfile != null)
            {
                var identity = new ClaimsIdentity(new[]
                   {
                        new Claim("id", userProfile.ID.ToString()),
                        new Claim("nickname", userProfile.Nickname),
                        new Claim("avatar", userProfile.Avatar),
                    }, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);
                if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return Redirect(returnUrl);
                }
            }
            else
            {
                ModelState.AddModelError("", "用户名或密码错误");
                return View(model);
            }
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Index","Home");
        }

    }
}