﻿using Raoke10086.Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Raoke10086.Admin.APIControllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        private readonly IProductSvc _productSvc;

        public ProductController(IProductSvc productSvc)
        {
            _productSvc = productSvc;
        }

        [HttpDelete, Route("{id}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(Guid id)
        {
            return Ok(_productSvc.Delete(User.Identity.GetUserID(), id));
        }

        [HttpPut, Route("{id}/listing")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Listing(Guid id)
        {
            return Ok(_productSvc.Listing(User.Identity.GetUserID(), id));
        }

        [HttpPut, Route("{id}/delisting")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delisting(Guid id)
        {
            return Ok(_productSvc.Delisting(User.Identity.GetUserID(), id));
        }
    }
}
